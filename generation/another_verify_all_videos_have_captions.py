#!/usr/bin/python3

import requests
import json
from tqdm import tqdm
from multiprocessing.dummy import Pool as ThreadPool
from yt_dlp import YoutubeDL

with open('keys.json') as keysFile:
    keys = json.load(keysFile)

keysIndex = 0

def request(url):
    key = keys[keysIndex]
    finalUrl = f'https://www.googleapis.com/youtube/v3/{url}&key={key}'
    data = requests.get(finalUrl).json()
    return data

def requestAllItems(url):
    pageToken = ''
    allItems = []
    while True:
        pageUrl = f'{url}&pageToken={pageToken}'
        data = request(pageUrl)
        items = data.get('items', [])
        allItems += items
        if not 'nextPageToken' in data:
            break
        pageToken = data['nextPageToken']
    return allItems

videos = []

channelVideosCommentsFilePath = 'channel_videos_comments.json'

with open(channelVideosCommentsFilePath) as channelVideosCommentsFile:
    channelVideosComments = json.load(channelVideosCommentsFile)
    for channelId in channelVideosComments:
        videos += channelVideosComments[channelId].keys()

videoAutomaticCaptions = {}

def treatVideo(video):
    print(video, sum(videoAutomaticCaptions[videoAutomaticCaption] == 1 for videoAutomaticCaption in videoAutomaticCaptions), sum(videoAutomaticCaptions[videoAutomaticCaption] == 0 for videoAutomaticCaption in videoAutomaticCaptions))

    with YoutubeDL() as ydl:
        # Unclear if `process = False` helps.
        # It seems to download `webpage`, `ios player API JSON`, `android player API JSON` and `m3u8 information`, is it actually the case and is it actually needed?
        automaticCaptions = ydl.extract_info(video, download = False, process = False)['automatic_captions']

    origAutomaticCaptionCount = sum(automaticCaption.endswith('-orig') for automaticCaption in automaticCaptions)
    videoAutomaticCaptions[video] = origAutomaticCaptionCount
    if not origAutomaticCaptionCount in [0, 1]:
        print('Error!', origAutomaticCaptionCount)
        exit(1)

for video in tqdm(videos):
    treatVideo(video)
#pool = ThreadPool(1)#len(videos))
#results = pool.map(treatVideo, videos)
