#!/usr/bin/python3

import json
import os
import gzip

with open('threads_work.json') as threadsWorkFile:
    threadsWork = json.load(threadsWorkFile)

for threadsWorkKey in threadsWork:
    if threadsWorkKey.isdigit():
        channelId, videoId = threadsWork[threadsWorkKey]
        gzipVideoIdFilePath = f'channels/{channelId}/{videoId}.json.gz'
        if os.path.isfile(gzipVideoIdFilePath):
            #print(f'Verifying {gzipVideoIdFilePath}')
            chunkSize = 10_000_000 # 10 MB

            corrupted = False
            with gzip.open(gzipVideoIdFilePath, 'rb') as f:
                try:
                    while f.read(chunkSize) != b'':
                        pass
                except (gzip.BadGzipFile, EOFError):
                    corrupted = True
                    print('Corrupted', gzipVideoIdFilePath)
            #if not corrupted:
            #    print('Not corrupted')
