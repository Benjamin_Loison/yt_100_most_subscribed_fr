#!/usr/bin/python3

import requests
import json
import os
import os.path
from multiprocessing.dummy import Pool as ThreadPool

channelsFolder = 'channels'

with open('channel_ids.json') as f:
    channelIds = json.load(f)

with open('keys.json') as keysFile:
    keys = json.load(keysFile)

keysIndex = 0

def request(url, index = 0):
    key = keys[keysIndex]
    finalUrl = f'https://www.googleapis.com/youtube/v3/{url}&key={key}'
    text = requests.get(finalUrl).text
    try:
        data = json.loads(text)
    except:
        if index > 0:
            print(f'text (finalUrl={finalUrl}, index={index})={text}!')
        return request(url, index + 1)
    if 'error' in data:
        reason = data['error']['errors'][0]['reason']
        if reason != 'commentsDisabled':
            if reason == 'videoNotFound':
                if index > 0:
                    print(f'text (finalUrl={finalUrl}, index={index})={text}!')
                return request(url, index + 1)
            elif reason == 'processingFailure':
                return request(url, index + 1)
            print(url, json.dumps(data, indent = 4))
            exit(1)
    return data

def requestAllItems(url):
    pageToken = ''
    allItems = []
    while True:
        pageUrl = f'{url}&pageToken={pageToken}'
        data = request(pageUrl)
        items = data.get('items', [])
        allItems += items
        if not 'nextPageToken' in data:
            break
        pageToken = data['nextPageToken']
    return allItems

def getChannelFolder(channelId):
    return f'{channelsFolder}/{channelId}'

channelVideosCommentsFilePath = 'channel_videos_comments.json'
channelVideos = []

if os.path.isfile(channelVideosCommentsFilePath):
    with open(channelVideosCommentsFilePath) as channelVideosCommentsFile:
        channelVideosComments = json.load(channelVideosCommentsFile)
        for channelId in channelVideosComments:
            channelVideos += [[channelId, videoId] for videoId in channelVideosComments[channelId]]
else:
    for channelId in channelIds[::-1]:
        print(f'{channelId=}')
        playlistId = 'UU' + channelId[2:]

        videos = requestAllItems(f'playlistItems?part=snippet&playlistId={playlistId}&maxResults=50')
        videoIds = set([video['snippet']['resourceId']['videoId'] for video in videos])
        print(f'{len(videoIds)=}')
        channelVideos += [[channelId, videoId] for videoId in videoIds]

channelIds = set([channelVideo[0] for channelVideo in channelVideos])
for channelId in channelIds:
    channelFolder = getChannelFolder(channelId)
    if not os.path.isdir(channelFolder):
        os.mkdir(channelFolder)

def treatVideo(channelVideo):
    channelId, videoId = channelVideo
    #print(f'{channelId=} {videoId=}')
    channelFolder = getChannelFolder(channelId)
    videoIdFilePath = f'{channelFolder}/{videoId}.json'
    if os.path.isfile(videoIdFilePath):
        try:
            with open(videoIdFilePath) as videoIdFile:
                json.load(f)
                return
        except:
            pass
    comments = requestAllItems(f'commentThreads?part=snippet&videoId={videoId}&maxResults=100')
    with open(videoIdFilePath, 'w') as f:
        json.dump(comments, f, indent = 4)

pool = ThreadPool(64)#len(channelVideos))
results = pool.map(treatVideo, channelVideos)

