#!/usr/bin/python3

import requests
from lxml import html
import json

url = 'https://socialblade.com/youtube/top/country/fr/mostsubscribed'
headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:121.0) Gecko/20100101 Firefox/121.0'
}
text = requests.get(url, headers = headers).text

tree = html.fromstring(text)
channelElements = tree.xpath('/html/body/div[10]/div[2]/div/div[3]/a')
channelIds = [channelElement.attrib['href'].replace('/youtube/channel/', '') for channelElement in channelElements]

with open('channel_ids.json', 'w') as f:
    json.dump(channelIds, f, indent = 4)