#!/usr/bin/python3

import gzip
import json
import csv
from tqdm import tqdm

'''
with open('dataset.tsv') as f:
    reader = csv.reader(f, delimiter='\t')
    for row in reader:
        print(row)
        break
'''

with gzip.open('videos.json.gz') as f:
    videos = json.load(f)

rows = [['item', 'description']]

for videoIndex, video in enumerate(tqdm(videos)):
    #id_ = videos[video]
    with open(f'captions/captions/first_256_words/{video}.txt') as f:
        rows += [[videoIndex + 1, f.read()]]#id_

with open('text.csv', 'w') as csvfile:
    spamwriter = csv.writer(csvfile)
    spamwriter.writerows(rows)
