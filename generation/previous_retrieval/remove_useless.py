#!/usr/bin/python3

import os
import zipfile

for channelFile in os.listdir()[7:]:
    if not channelFile.startswith('UCWeg'):
        continue
    print(channelFile)
    temporaryChannelFile = channelFile + '.tmp'
    with zipfile.ZipFile(channelFile) as originalZip:
        nameList = originalZip.namelist()
        with originalZip.open('requests/urls.txt') as f:
            lines = [line.decode('ascii') for line in f.read().splitlines()]
            for lineIndex, line in enumerate(lines):
                if not line.startswith('commentThreads?'):
                    #print(line)
                    nameList.remove(f'requests/{lineIndex}.json')
        with zipfile.ZipFile(temporaryChannelFile, 'w', zipfile.ZIP_DEFLATED, compresslevel = 6) as newZip:
            for file in nameList:
                if not file.startswith('captions/') and file != 'requests/':
                    data = originalZip.read(file)
                    newZip.writestr(file.replace('requests/', '') if file != 'requests/' else file, data)
    os.replace(temporaryChannelFile, channelFile)
    break
