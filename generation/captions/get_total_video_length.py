#!/usr/bin/python3

import json
import requests
from tqdm import tqdm
import re
import gzip

channelVideosCommentsFilePath = 'channel_videos_comments.json'

videos = []

with open(channelVideosCommentsFilePath) as channelVideosCommentsFile:
    channelVideosComments = json.load(channelVideosCommentsFile)
    for channelId in channelVideosComments:
        videos += channelVideosComments[channelId].keys()

KEY = 'AIzaSyA...'

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

totalLength = 0

videoChunks = list(chunks(videos, 50))
for videoChunk in tqdm(videoChunks):
    url = f'https://www.googleapis.com/youtube/v3/videos?part=contentDetails,id,liveStreamingDetails,localizations,player,recordingDetails,snippet,statistics,status,topicDetails&id={",".join(videoChunk)}&key={KEY}'
    data = requests.get(url).json()
    for item in data['items']:
        id_ = item['id']
        durationStr = item['contentDetails']['duration']
        print(id_, durationStr)
        with gzip.open(f'yt_data_api_v3_info/{id_}.json.gz', 'wt', encoding = 'utf-8') as f:
            json.dump(item, f, indent = 4)
        if durationStr == 'P0D':
            continue
        #secondsMatch = re.match('PT(\d{,2})S', durationStr)
        #minutesMatch = re.match('PT(\d{,2})M(?:(\d{,2})S)?', durationStr)
        match_ = re.match('P(?:(\d+)D)?T(?:(\d{,2})H)?(?:(\d{,2})M)?(?:(\d{,2})S)?', durationStr)
        #hoursMatch = re.match('PT(\d{,2})H(?:(\d{,2}))?M(?:(\d{,2})S)?', durationStr)
        #hoursSecondsMatch = re.match('PT(\d{,2})H(?:(\d{,2})S)?', durationStr)
        '''
        if secondsMatch:
            groups = secondsMatch.groups()
            totalLength += int(groups[0])
        elif minutesMatch:
            groups = minutesMatch.groups()
            totalLength += int(groups[0]) * 60 + int(groups[1] or 0)
        '''
        #elif hoursMatch:
        if match_:
            groups = match_.groups()
            totalLength += int(groups[0] or 0) * 24 * 60 * 60 + int(groups[1] or 0) * 60 * 60 + int(groups[2] or 0) * 60 + int(groups[3] or 0)
        #elif hoursSecondsMatch:
        #    groups = hoursSecondsMatch.groups()
        #    totalLength += int(groups[0]) * 60 * 60 + int(groups[1])
        else:
            print('Not matching!', durationStr)
            exit(1)
    print(totalLength)

