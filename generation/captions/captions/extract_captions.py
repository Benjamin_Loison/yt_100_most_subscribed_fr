#!/usr/bin/python3

import gzip
import json
from tqdm import tqdm
from urllib.parse import urlparse, parse_qs
import time
import urllib
import urllib.request
import urllib.parse as ul
import re
import socks
import socket
import requests
import os
import os.path
from yt_dlp import YoutubeDL
import yt_dlp
import random

from datetime import datetime
import builtins as __builtin__

def print(*args, **kwargs):
    # Adding new arguments to the print function signature is probably a bad idea.
    # Instead consider testing if custom argument keywords are present in kwargs.
    __builtin__.print(datetime.now())
    return __builtin__.print(*args, **kwargs)

# If have again the issue that nothing seems to be happening, then could log `print`s in a specific file to have debugging, otherwise we randomly have it.
# Maybe this issue is just a filling, a `vim` shows the file updates.

def setTor(torStatus):
    if torStatus:
        socks.set_default_proxy(socks.SOCKS5, '127.0.0.1', 9050)
    else:
        socks.set_default_proxy()

# To stay up-to-date:
print('Retrieving exit node ips...')
urllib.request.urlretrieve('https://check.torproject.org/torbulkexitlist', 'torbulkexitlist')
print('Retrieved exit node ips!')

with open('torbulkexitlist') as f:
    torExitNodeIps = f.read().splitlines()

random.shuffle(torExitNodeIps)
torExitNodeIndex = 0
torrcFilePath = '/etc/tor/torrc'

with open('caption_extractions.json') as f:
    data = json.load(f)

def getIp():
    print('Retrieving IP...')
    try:
        ip = requests.get('https://checkip.amazonaws.com').text.strip()
    except Exception as e:
        print(f'`getIp` exception: {e}')
        # Uncomment if it seems appropriate.
        #changeTorIp()
        return getIp()
    print('IP retrieved!')
    return ip 

def changeTorIp():
    global torExitNodeIndex
    #print(f'Current IP: {getIp()}')
    with open(torrcFilePath) as f:
        lines = f.read().splitlines()
    print(f'Current IP: {lines[-1].split()[1]}')
    newIp = torExitNodeIps[torExitNodeIndex]
    lines[-1] = f'ExitNodes {newIp}'
    with open(torrcFilePath, 'w') as f:
        f.write('\n'.join(lines))
    print('Reloading...')
    os.system('sudo service tor reload')
    print('Reloaded!')
    print(f'New IP: {newIp}')
    torExitNodeIndex += 1

setTor(True)
socket.socket = socks.socksocket

changeTorIp()

videoIds = list(data.keys())
random.shuffle(videoIds)
files = tqdm(videoIds)

for file in files:
    filePath = f'captions/{file}.ttml'
    if os.path.isfile(filePath):
        continue
    '''
    url = ul.unquote_plus(data[file])
    parameters = parse_qs(urlparse(url).query)
    if 'expire' in parameters:
        expirationStr = parameters['expire'][0]
    else:
        match_ = re.match('https://manifest\\.googlevideo\\.com/api/manifest/hls_timedtext_playlist/expire/(\d+)/ei', url)
        if match_:
            expirationStr = match_.group(1)
        else:
            print('Bad URL!')
            exit(1)
    expiration = int(expirationStr)
    if time.time() <= expiration:
    '''
    setTor(False)
    print('Retrieving video info...')
    with YoutubeDL() as ydl:
        try:
            info = ydl.extract_info(file, download = False, process = False)
        except (yt_dlp.utils.ExtractorError, yt_dlp.utils.DownloadError) as e:
            print('Exception on info retrieval!', e)
            continue
    print('Retrieved video info!')
    setTor(True)
    automaticCaptions = info.get('automatic_captions', {})
    for format_ in automaticCaptions.get('en', []):
        if format_['ext'] == 'ttml':
            url = format_['url']
            cooldown = 1
            while True:
                #try:
                #urllib.request.urlretrieve(url, filePath)
                print('Retrieving caption...')
                while True:
                    try:
                        response = requests.get(url, timeout = 60)
                        break
                    except (requests.exceptions.SSLError, requests.exceptions.ConnectionError, requests.exceptions.ChunkedEncodingError, requests.exceptions.ReadTimeout) as e:#Exception as e: #requests.exceptions.SSLError:
                        #print('Retrying after SSL error')
                        print('Exception: ', e, '!')
                        eStr = str(e)
                        if 'TTL expired' in eStr or 'Read timed out' in eStr:
                            changeTorIp()
                        continue
                print('Caption retrieved!')
                if response.status_code != 200:
                    print(f'Detected as too many requests!')
                    changeTorIp()
                    continue
                text = response.text
                with open(filePath, 'w') as f:
                    f.write(text)
                break
                '''
                except urllib.error.HTTPError as exception:
                    if exception.reason == 'Too Many Requests':
                        print(f'Too many requests... Sleeping for {cooldown} seconds!')
                        time.sleep(cooldown)
                        cooldown *= 2
                        continue
                    print('HTTP error!', exception.reason)
                    exit(1)
                '''
            break
