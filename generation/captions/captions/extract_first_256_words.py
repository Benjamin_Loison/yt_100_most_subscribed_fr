#!../venv/bin/python3

import os
import webvtt
import re
from tqdm import tqdm
import os.path

vttCaptionsFolder = 'captions_vtt'

error = 0

vttCaptions = os.listdir(vttCaptionsFolder)
for file in tqdm(vttCaptions):
    print(file)
    txtFilePath = f'first_256_words/{file.replace(".vtt", "")}.txt'
    if os.path.exists(txtFilePath):
        continue
    try:
        text = re.sub(' +', ' ', ' '.join([caption.text for caption in webvtt.read(f'{vttCaptionsFolder}/{file}')]))
    except:
        error += 1
        continue
    #print(text)
    words = text.split()
    #if len(words) < 256:
    #    continue
    first256Words = ' '.join(words[:256])#re.findall(r'\w+', text)
    #print(first256Words)
    with open(txtFilePath, 'w') as f:
        f.write(first256Words)
    #break

print(f'{error}/{len(vttCaptions)}') # 2 / 91308
