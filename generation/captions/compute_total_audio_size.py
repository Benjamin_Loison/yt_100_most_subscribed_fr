#!/usr/bin/python3

import os
import gzip
import json
from tqdm import tqdm

os.chdir('infos')

totalAudioSize = 0

for file in tqdm(os.listdir('.')):
    with gzip.open(file) as f:
        data = json.load(f)
    #print(json.dumps(data, indent = 4))
    lowestFileSize = None
    for format_ in data.get('formats', []):
        #print(format_['format_note'])
        if format_.get('acodec', 'none') != 'none' and format_['vcodec'] == 'none':
            #print(json.dumps(format_, indent = 4))
            fileSize = format_['filesize']
            if lowestFileSize is None or fileSize < lowestFileSize:
                lowestFileSize = fileSize
    totalAudioSize += fileSize
    print(totalAudioSize)
