#!/usr/bin/python3

import os
import gzip
import json
from tqdm import tqdm
from urllib.parse import urlparse, parse_qs
import time
import urllib
import urllib.request
import urllib.parse as ul
import re

infosFolder = 'infos'

files = tqdm(os.listdir(infosFolder))

for file in files:
    with gzip.open(f'{infosFolder}/{file}') as f:
        data = json.load(f)
    automaticCaptions = data.get('automatic_captions', {})
    ens = automaticCaptions.get('en', [])
    for ensElement in ens:
        if ensElement['ext'] == 'vtt':
            url = ul.unquote_plus(ensElement['url'])
            parameters = parse_qs(urlparse(url).query)
            if 'expire' in parameters:
                expirationStr = parameters['expire'][0]
            else:
                match_ = re.match('https://manifest\\.googlevideo\\.com/api/manifest/hls_timedtext_playlist/expire/(\d+)/ei', url)
                if match_:
                    expirationStr = match_.group(1)
                else:
                    print('Bad URL!')
                    exit(1)
            expiration = int(expirationStr)
            if time.time() <= expiration:
                cooldown = 1
                while True:
                    try:
                        urllib.request.urlretrieve(url, f'captions/{file.replace(".json.gz", ".vtt")}')
                        break
                    except urllib.error.HTTPError as exception:
                        if exception.reason == 'Too Many Requests':
                            print(f'Too many requests... Sleeping for {cooldown} seconds!')
                            time.sleep(cooldown)
                            cooldown *= 2
                            continue
                        print('HTTP error!', exception.reason)
                        exit(1)
