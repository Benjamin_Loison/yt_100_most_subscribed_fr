#!/usr/bin/python3

import os
import gzip
import json
from tqdm import tqdm
from urllib.parse import urlparse, parse_qs
import time
import urllib
import urllib.request
import urllib.parse as ul
import re

infosFolder = 'infos'

interestedIn = {}#[]

files = tqdm(os.listdir(infosFolder))

for file in files:
    with gzip.open(f'{infosFolder}/{file}') as f:
        data = json.load(f)
    automaticCaptions = data.get('automatic_captions', {})
    ens = automaticCaptions.get('en', [])
    for ensElement in ens:
        if ensElement['ext'] == 'ttml':#'vtt':
            videoId = file.replace('.json.gz', '')
            #interestedIn += [videoId]
            interestedIn[videoId] = ensElement['url']
            break

with open('caption_extractions.json', 'w') as f:
    json.dump(interestedIn, f, indent = 4)
