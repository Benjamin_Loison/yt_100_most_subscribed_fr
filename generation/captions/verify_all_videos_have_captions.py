#!/usr/bin/python3

import requests
import json
from multiprocessing.dummy import Pool as ThreadPool
import yt_dlp
from yt_dlp import YoutubeDL
from threading import Lock
import gzip
import os.path
from random import shuffle
import time

videos = []
beginTime = time.time()

channelVideosCommentsFilePath = 'channel_videos_comments.json'

with open(channelVideosCommentsFilePath) as channelVideosCommentsFile:
    channelVideosComments = json.load(channelVideosCommentsFile)
    for channelId in channelVideosComments:
        videos += channelVideosComments[channelId].keys()

videoAutomaticCaptions = {}
mutex = Lock()

def treatVideo(video):
    with mutex:
        print(video, sum(videoAutomaticCaptions[videoAutomaticCaption] == 1 for videoAutomaticCaption in videoAutomaticCaptions), sum(videoAutomaticCaptions[videoAutomaticCaption] == 0 for videoAutomaticCaption in videoAutomaticCaptions), len(videoAutomaticCaptions) / (time.time() - beginTime))

    videoFilePath = f'infos/{video}.json.gz'

    if os.path.isfile(videoFilePath):
        with gzip.open(videoFilePath) as f:
            info = json.loads(f.read().decode('utf-8'))
    else:
        with YoutubeDL() as ydl:
            # Unclear if `process = False` helps.
            # It seems to download `webpage`, `ios player API JSON`, `android player API JSON` and `m3u8 information`, is it actually the case and is it actually needed?
            try:
                info = ydl.extract_info(video, download = False, process = False)
            except (yt_dlp.utils.ExtractorError, yt_dlp.utils.DownloadError):
                return
        beforeWritingTime = time.time()
        with gzip.open(videoFilePath, 'wt', encoding = 'utf-8') as f:
            json.dump(info, f, indent = 4)
        afterWritingTime = time.time()
        with mutex:
            print(f'Writing took {afterWritingTime - beforeWritingTime} seconds!')
    automaticCaptions = info['automatic_captions']

    origAutomaticCaptionCount = sum(automaticCaption.endswith('-orig') for automaticCaption in automaticCaptions)
    with mutex:
        videoAutomaticCaptions[video] = origAutomaticCaptionCount
    if not origAutomaticCaptionCount in [0, 1]:
        print('Error!', origAutomaticCaptionCount)
        exit(1)

shuffle(videos)
pool = ThreadPool(1)
results = pool.map(treatVideo, videos)
