#!/usr/bin/python3

import os
import csv
from tqdm import tqdm
import gzip
import json
import random
from bidict import bidict

# To optimize memory, as we assume more than two comments per author and per video.
# Cf https://github.com/Benjamin-Loison/bidict/issues/1.
memoryAuthorChannelIds = bidict()
memoryVideos = bidict()

# To avoid training on single like and predicting on single associated dislike.
authorComments = {}

# To ponderate randomness when select videos to dislike.
videoComments = {}

# Only consider videos with the final captions processing, to have captions for each video.
for file in tqdm(os.listdir('captions/captions/first_256_words')):
    video = os.path.splitext(file)[0]
    # Only consider one comment per author per video, to not have to predict if this author likes this video, as the network was already trained knowing that it likes it.
    authorAlreadyCommentedVideo = set()
    with gzip.open(f'comments/videos/{video}.json.gz') as f:
        items = json.load(f)
        for item in items:
            snippet = item['snippet']['topLevelComment']['snippet']
            if 'authorChannelId' in snippet:
                authorChannelId = snippet['authorChannelId']['value']
                comment = item['id']
                if not authorChannelId in memoryAuthorChannelIds:
                    memoryAuthorChannelIds[authorChannelId] = len(memoryAuthorChannelIds)
                if not video in memoryVideos:
                    memoryVideos[video] = len(memoryVideos)
                memoryAuthorChannelId = memoryAuthorChannelIds[authorChannelId]
                memoryVideo = memoryVideos[video]
                videoComments[memoryVideo] = videoComments.get(memoryVideo, 0) + 1
                memoryAuthorChannelIdVideo = (memoryAuthorChannelId, memoryVideo)
                if not memoryAuthorChannelIdVideo in authorAlreadyCommentedVideo:
                    authorAlreadyCommentedVideo.add(memoryAuthorChannelIdVideo)
                else:
                    continue
                authorComments[memoryAuthorChannelId] = authorComments.get(memoryAuthorChannelId, []) + [[memoryVideo, comment]]

# Remove channels with a single comment.
for memoryAuthorChannelId in list(authorComments):
    if len(authorComments[memoryAuthorChannelId]) < 2:
        del authorComments[memoryAuthorChannelId]

# Note that due to current sampling, may have more rows than intended.
rowsLen = 500_000

# Sample channels according to their number of comments.
sampledChannels = random.sample(list(authorComments.keys()), counts = [len(authorComment) for authorComment in authorComments.values()], k = rowsLen)

def getMapCount(list_):
    map_ = {}
    for element in list_:
        map_[element] = map_.get(element, 0) + 1
    return map_

sampledChannelsCounts = getMapCount(sampledChannels)

rows = []

# Get `rows` with at least 2 comments per channel.
for sampledChannelCount in sampledChannelsCounts:
    sampleComments = random.sample(authorComments[sampledChannelCount], k = max(sampledChannelsCounts[sampledChannelCount], 2))
    rows += [[sampledChannelCount] + sampleComment for sampleComment in sampleComments]

# Once have sampled, expand values.
for rowIndex, row in enumerate(rows):
    memoryAuthorChannelId, memoryVideo, comment = row
    rows[rowIndex] = [memoryAuthorChannelIds.inverse[memoryAuthorChannelId], memoryVideos.inverse[memoryVideo], comment]

videoComments = {memoryVideos.inverse[video]: videoComments[video] for video in videoComments}

# Define final `users` ids.
users = {}
for row in rows:
    user = row[0]
    if not user in users:
        users[user] = len(users)

# Offset `videos` with `len(users)` and add the like rating.
usersLen = len(users)
videos = {}
for rowIndex, row in enumerate(rows):
    user, video, comment = row
    if not video in videos:
        videos[video] = len(videos) + usersLen
    rows[rowIndex] = [users[user], videos[video], comment, 1]

# Add random dislikes.
# Should add a data structure like a `dict` to avoid contradictions such as having a user liking and disliking a given video.
# For the moment we rely on a probabilistic approach to avoid these contradictions and to in average avoid disliking *nearest* videos.
# Pay attention not to have an infinite loop as for each array element we add another.
# Assume `.keys` and `.values` return results in the same order.
videoCommentNumber = [videoComments[video] for video in videos]
videoIds = list(videos.values())
# Do not use `rowsLen`, as it is probably not equal to `len(rows)`.
for rowIndex in tqdm(range(len(rows))):
    user, video, comment, _rating = rows[rowIndex]
    # `rd` stands for *random dislike*.
    # This allows to have unique *random dislike* ids.
    # Could maybe optimize by using `k=len(rows)`.
    rows += [[user, random.choices(videoIds, videoCommentNumber, k = 1)[0], f'rd_{comment}', 0]]

# To avoid any bias due to first having all likes and then all dislikes.
random.shuffle(rows)

# Add comment bijection table.
comments = []
for rowIndex, [user, video, comment, rating] in enumerate(rows):
    comments += [comment]
    rows[rowIndex] = [user, video, rating]

# Write bijection tables to lookup on YouTube each rating source.
def dump(fileName, data):
    with gzip.open(f'{fileName}.json.gz', 'wt', encoding = 'utf-8') as f:
        json.dump(data, f, indent = 4)

dump('comments', comments)
dump('videos', videos)
dump('users', users)

# Write actual dataset.
with open('dataset.tsv', 'w') as f:
    writer = csv.writer(f, delimiter = '\t')
    writer.writerows(rows)
