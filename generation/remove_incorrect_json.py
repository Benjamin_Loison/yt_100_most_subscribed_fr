#!/usr/bin/python3

import json
import os
import gzip
from tqdm import tqdm

with open('channel_ids.json') as channelIdsFile:
    channelIds = json.load(channelIdsFile)

os.chdir('channels')

channelIdsProgressBar = tqdm(channelIds)
for channelId in channelIdsProgressBar:
    #print(channelId)
    channelIdsProgressBar.set_description(channelId)
    for videoFileName in tqdm(os.listdir(channelId), desc=channelId):
        videoFilePath = f'{channelId}/{videoFileName}'
        # Note that have to read them completely, as as far as I know cannot just read the decompressed last 2 characters of a `.gz`.
        with gzip.open(videoFilePath) as videoFile:
            try:
                video = json.load(videoFile)
            except json.decoder.JSONDecodeError:
                tqdm.write(videoFileName)
