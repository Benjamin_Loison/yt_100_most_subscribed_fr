#!/usr/bin/python3

import os
import zipfile
import json
from tqdm import tqdm

with open('/mnt/HDD0/youtube/channel_ids.json') as channelIdsFile:
    channelIds = json.load(channelIdsFile)

authors = {}

for channelId in tqdm(channelIds):
    channelFile = f'{channelId}.zip'
    print(channelFile, len([authorCommentsNumber for authorCommentsNumber in authors.values() if authorCommentsNumber > 1]))
    if os.path.isfile(channelFile):
        with zipfile.ZipFile(channelFile) as zipFile:
            with zipFile.open('requests/urls.txt') as f:
                lines = [line.decode('ascii') for line in f.read().splitlines()]
                for lineIndex, line in enumerate(tqdm(lines)):
                    #if lineIndex % 1_000 == 0:
                    #    print(f'{lineIndex} / {len(lines)}')
                    # Could alternatively list videos thanks to `playlistItems?` and then find the files associated to each video but assuming the initial retrieval algorithm worked fine, then it is not necessary.
                    if line.startswith('commentThreads?'):
                        with zipFile.open(f'requests/{lineIndex}.json') as requestFile:
                            data = json.load(requestFile)
                            #print(json.dumps(data, indent = 4))
                            items = data.get('items', [])
                            for item in items:
                                snippet = item['snippet']
                                # As we are only interested in video comments:
                                #if 'videoId' in snippet:
                                #    print(json.dumps(snippet, indent = 4))
                                if 'topLevelComment' in snippet:
                                    snippet = snippet['topLevelComment']['snippet']
                                    if 'authorChannelId' in snippet:
                                        channelId = snippet['authorChannelId']['value']
                                        authors[channelId] = authors.get(channelId, 0) + 1
                                        #print(authors[channelId])
    else:
        print('File does not exist!')

with open('authors.json', 'w') as authorsFile:
    json.dump(authors, authorsFile, indent = 4)
